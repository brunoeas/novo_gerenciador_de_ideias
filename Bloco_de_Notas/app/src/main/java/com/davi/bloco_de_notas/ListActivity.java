package com.davi.bloco_de_notas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    static List<String> itens = new ArrayList<>();
    List<IdeiaModel> ideias = new DBHelper(this).getAllIdeias();
    static ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ListView listView = findViewById(R.id.listView);

        int pos = 0;
        for (IdeiaModel ideiaFor : ideias) {
            ideiaFor.setPosicao(pos);
            itens.add(ideiaFor.getTitulo());
        }

        adapter = new ArrayAdapter( this, android.R.layout.simple_list_item_1, itens);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putLong("id", retornaModelPelaPosicao(i).getId());
                bundle.putString("titulo", retornaModelPelaPosicao(i).getTitulo());
                bundle.putString("objetivo", retornaModelPelaPosicao(i).getObjetivo());
                bundle.putString("descricao", retornaModelPelaPosicao(i).getDescricao());

                Intent intent = new Intent(getApplicationContext(), AddeEditActivity.class);
                intent.putExtra("objetoIdeia", bundle);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                alertDialogBuilder.setTitle("Titulo");
                alertDialogBuilder.setMessage("Tem certeza que deseja excluir esta ideia?").setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteIdeia(position);
                    }
                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    private void deleteIdeia(int posicao) {
        SQLiteDatabase db = new DBHelper(getApplicationContext()).getWritableDatabase();
        db.delete(IdeiasContract.IdeiasEntry.TABLE_NAME, "ID = ?", new String[] { String.valueOf(retornaModelPelaPosicao(posicao)) });
    }

    private IdeiaModel retornaModelPelaPosicao(int posicao) {
        for (IdeiaModel ideiaFor : ideias) {
            if (ideiaFor.getPosicao() == posicao) {
                return ideiaFor;
            }
        }
        return new IdeiaModel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.add_ideia) {
            Intent intent = new Intent(getApplicationContext(), AddeEditActivity.class);
            startActivity(intent);

            return true;
        }
        return false;
    }
}