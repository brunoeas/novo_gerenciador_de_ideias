package com.davi.bloco_de_notas;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ideias_data";
    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "+IdeiasContract.IdeiasEntry.TABLE_NAME+"("+ IdeiasContract.IdeiasEntry.COLUMN_ID+" INTEGER PRIMARY KEY, "+IdeiasContract.IdeiasEntry.COLUMN_TITULO+" TEXT, "+IdeiasContract.IdeiasEntry.COLUMN_OBJETIVO+" TEXT, "+IdeiasContract.IdeiasEntry.COLUMN_DESCRICAO+" TEXT)";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "+IdeiasContract.IdeiasEntry.TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public List<IdeiaModel> getAllIdeias() {
        List<IdeiaModel> ideias = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+IdeiasContract.IdeiasEntry.TABLE_NAME, null);

        if (cursor.moveToFirst()) {
            do {
                IdeiaModel ideia = new IdeiaModel();
                ideia.setId(Long.parseLong(cursor.getString(0)));
                ideia.setTitulo(cursor.getString(1));
                ideia.setObjetivo(cursor.getString(2));
                ideia.setDescricao(cursor.getString(3));

                ideias.add(ideia);
            } while (cursor.moveToNext());
        }

        return ideias;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}