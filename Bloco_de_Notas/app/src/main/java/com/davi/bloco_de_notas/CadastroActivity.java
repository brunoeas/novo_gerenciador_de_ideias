package com.davi.bloco_de_notas;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class CadastroActivity extends AppCompatActivity {
    private EditText emailNovo;
    private EditText nomeNovo;
    private EditText senhaNova;
    private EditText senhaConfirmacao;

    public void verificaUsuario(View view) {
        if (validador()) {
            usuarioExiste(emailNovo.getText().toString(), this);
        }
    }

    public void usuarioExiste(final String emailDigitado, Context contexto) {
        String URL = "https://gerenciadordeideias-43f0e.firebaseio.com/users.json";

        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean aux = false;
                            Iterator keys = response.keys();
                            while(keys.hasNext()) {
                                String keyAtual = (String)keys.next();
                                JSONObject objetoAtual = (JSONObject)response.get(keyAtual);

                                Iterator keyInterna = objetoAtual.keys();
                                while (keyInterna.hasNext()) {
                                    String atributoAtual = (String)keyInterna.next();

                                    if (atributoAtual.equals("email")) {
                                        if (objetoAtual.getString("email").equals(emailDigitado)) {
                                            mostraMsg(getApplicationContext(), "O Email digitado ja existe!");
                                            aux = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!aux) {
                                criaUsuario();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());

                    }
                }
        );

        requestQueue.add(jsonObjectRequest);
    }

    public void mostraMsg(Context contexto ,String msg) {
        Toast.makeText(contexto, msg, Toast.LENGTH_SHORT).show();
    }

    private void criaUsuario() {
        JSONObject usuario = new JSONObject();
        try {
            usuario.put("email", emailNovo.getText().toString());
            usuario.put("nome", nomeNovo.getText().toString());
            usuario.put("senha", senhaNova.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mostraMsg(getApplicationContext(),"Usuario foi criado com sucesso!");
        new DAO().addIdNovo(this, usuario);

        Intent intent = new Intent(getApplicationContext(), ListActivity.class);
        startActivity(intent);
    }

    public boolean validador() {
        String msg = "";
        boolean retorno = true;

        if (emailNovo.getText().toString().length() < 8) {
            msg = "O Email deve ter no minimo 8 digitos!";
            retorno = false;
        }

        if (nomeNovo.getText().toString().length() < 2) {
            msg = "O Nome deve ter no minimo 2 digitos!";
            retorno = false;
        }

        if (senhaNova.getText().toString().length() < 6) {
            msg = "A senha deve ter no minimo 6 digitos!";
            retorno = false;
        }

        if (!senhaNova.getText().toString().equals(senhaConfirmacao.getText().toString())) {
            msg = "As senhas não coincidem!";
            retorno = false;
        }

        if (emailNovo.getText().toString().equals("") || nomeNovo.getText().toString().equals("") || senhaNova.getText().toString().equals("") || senhaConfirmacao.getText().toString().equals("")) {
            msg = "Todos os campos devem ser preenchidos!";
            retorno = false;
        }

        if (!msg.equals("")) {
            mostraMsg(getApplicationContext(), msg);
        }
        return retorno;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        emailNovo = findViewById(R.id.emailNovo);
        nomeNovo = findViewById(R.id.nomeNovo);
        senhaNova = findViewById(R.id.senhaNova);
        senhaConfirmacao = findViewById(R.id.senhaConfirmacao);
    }
}