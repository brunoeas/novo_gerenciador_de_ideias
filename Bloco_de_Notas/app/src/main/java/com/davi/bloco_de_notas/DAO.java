package com.davi.bloco_de_notas;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DAO {

    private void addUser(Context contexto, JSONObject usuario) {
        String URL = "https://gerenciadordeideias-43f0e.firebaseio.com/users.json";

        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URL,
                usuario,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Sucesso!", "Usuario foi criado com sucesso!");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());

                    }
                }
        );

        requestQueue.add(jsonObjectRequest);
    }

    public void addIdNovo(final Context contexto, final JSONObject user) {
        Log.i("teste", "entrou no addIdNovo()");
        String URL = "https://gerenciadordeideias-43f0e.firebaseio.com/users.json";

        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Integer contId = 1;
                        try {
                            Iterator keys = response.keys();
                            while(keys.hasNext()) {
                                contId++;
                                keys.next();
                            }

                            user.put("id", contId);
                            addUser(contexto, user);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());

                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }
}