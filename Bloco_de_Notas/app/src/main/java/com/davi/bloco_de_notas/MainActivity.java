package com.davi.bloco_de_notas;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity {
    private EditText email;
    private EditText senha;

    public void irParaCadastro(View view) {
        Intent intent = new Intent(getApplicationContext(), CadastroActivity.class);
        startActivity(intent);
    }

    public void logar(View view) {
        verificaLogin(this, email.getText().toString(), senha.getText().toString());
    }

    private void verificaLogin(Context contexto, final String email, final String senha) {
        String URL = "https://gerenciadordeideias-43f0e.firebaseio.com/users.json";

        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean validador1 = false, validador2 = false;
                            String nomeUsuario = "";
                            Iterator keys = response.keys();
                            while (keys.hasNext()) {
                                String keyAtual = (String) keys.next();
                                JSONObject objetoAtual = (JSONObject) response.get(keyAtual);

                                Iterator keyInterna = objetoAtual.keys();
                                while (keyInterna.hasNext()) {
                                    String atributoAtual = (String) keyInterna.next();

                                    switch (atributoAtual) {
                                        case "email":
                                            if (objetoAtual.getString("email").equals(email)) {
                                                validador1 = true;
                                            }
                                            break;

                                        case "senha":
                                            if (objetoAtual.getString("senha").equals(senha)) {
                                                validador2 = true;
                                            }
                                            break;
                                    }

                                    if (validador1 && validador2) {
                                        if (atributoAtual.equals("nome")) {
                                            nomeUsuario = objetoAtual.getString("nome");
                                        }
                                        break;
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Email ou Senha inválidos!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            if (validador1 && validador2) {
                                Toast.makeText(getApplicationContext(), "Seja Bem-vindo(a) " + nomeUsuario + "!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());

                    }
                }
        );

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = findViewById(R.id.emailLogin);
        senha = findViewById(R.id.senhaLogin);
    }
}