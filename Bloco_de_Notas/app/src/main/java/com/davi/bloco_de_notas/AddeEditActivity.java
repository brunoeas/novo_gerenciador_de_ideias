package com.davi.bloco_de_notas;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class AddeEditActivity extends AppCompatActivity {
    private EditText titulo;
    private EditText objetivo;
    private EditText descricao;
    int pos = ListActivity.itens.size();
    Long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adde_edit);

        titulo = findViewById(R.id.titulo);
        objetivo = findViewById(R.id.objetivo);
        descricao = findViewById(R.id.descricao);

        Intent intent = new Intent();
        Bundle bundle = intent.getBundleExtra("objetoIdeia");

        if (bundle != null) {
            titulo.setText(bundle.getString("titulo"));
            objetivo.setText(bundle.getString("objetivo"));
            descricao.setText(bundle.getString("descricao"));
            id = bundle.getLong("id");
        }else {
            ListActivity.itens.add(titulo.getText().toString());
            pos += 1;
            id = addNovaIdeia(titulo.getText().toString(), objetivo.getText().toString(), descricao.getText().toString());
            ListActivity.adapter.notifyDataSetChanged();
        }

        titulo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ListActivity.itens.set(pos, String.valueOf(s));
                ListActivity.adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
                atualizaIdeia(1, s.toString());
            }
        });

        objetivo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                atualizaIdeia(2, s.toString());
            }
        });

        descricao.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                atualizaIdeia(3, s.toString());
            }
        });
    }

    private Long addNovaIdeia(String titulo, String objetivo, String descricao) {
        DBHelper dbHelper = new DBHelper(getApplicationContext());

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IdeiasContract.IdeiasEntry.COLUMN_TITULO, titulo);
        values.put(IdeiasContract.IdeiasEntry.COLUMN_OBJETIVO, objetivo);
        values.put(IdeiasContract.IdeiasEntry.COLUMN_DESCRICAO, descricao);

        return db.insert(IdeiasContract.IdeiasEntry.TABLE_NAME, null, values);
    }

    private void atualizaIdeia(int codigoValidacao, String valor) {
        DBHelper dbHelper = new DBHelper(getApplicationContext());

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (codigoValidacao == 1) {
            values.put("titulo", valor);
        }else if (codigoValidacao == 2) {
            values.put("objetivo", valor);
        }else {
            values.put("descricao", valor);
        }

        db.update(IdeiasContract.IdeiasEntry.TABLE_NAME, values, "id = ?", new String[] { String.valueOf(id) });
    }
}
