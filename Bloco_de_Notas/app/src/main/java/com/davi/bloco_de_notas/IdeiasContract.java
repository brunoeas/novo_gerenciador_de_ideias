package com.davi.bloco_de_notas;

import android.provider.BaseColumns;

public final class IdeiasContract {
    private IdeiasContract() {
    }

    public static class IdeiasEntry implements BaseColumns {
        public static String TABLE_NAME = "ideias";
        public static String COLUMN_ID = "id";
        public static String COLUMN_TITULO = "titulo";
        public static String COLUMN_OBJETIVO = "objetivo";
        public static String COLUMN_DESCRICAO = "descricao";
    }
}